/*
AUTOR:castro valino, pablo:pablo.castro1
AUTOR:chavarria teijeiro, marcos:pablo.castro1
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include <sched.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <linux/sched.h>
#include <pwd.h>
#include <signal.h>


#ifndef PATH_MAX
#define PATH_MAX 1024
#endif

#define MAXCOMANDO 1024
#define INI_LIST 20
#define BUFFER_SIZE 2048
#define MAX_NAME 512
#define MARCA_MEMFS 337478386
#define MEMDUMP_LINEA 25

/*lista comandos*/

#define COMMAND_NOT_FOUND 0
#define FIN 1
#define AUTORES 2
#define PID 3
#define PWD 4
#define CD 5
#define PATH 6
#define ENVIRON 7
#define FORK 8
#define EXECUTE 9
#define BACKGROUND 10
#define JOBS 11
#define MEM 12
#define IOMEM 13
#define MEMDUMP 14
#define RECURSE 15
#define MEMFSMK 16
#define MEMFSCP 17
#define MEMFSMV 18
#define MEMFSRM 19
#define MEMFSLS 20
#define RMKEY 21
#define PRIORITY 22
#define POSIXPRI 23
#define SIGPROCMASK 24
#define SIGALTSTACK 25
#define SIGACTION 26
#define LIMITRESEND 27
#define SIGCOUNT 28
#define BUCLE 29
#define SEGMENTATION 30
#define UID 31
#define SIGINFO 32
#define PIPE 33
#define BACKPIPE 34

/*tipos lista*/
#define LISTA_PATH 0
#define LISTA_JOBS 1
#define LISTA_MALLOC 2
#define LISTA_MMAP 3
#define LISTA_SHARED 4

/*tipos environ*/
#define T_ENVIRON 0
#define T_MAIN 1

/*estado proceso*/
#define ACTIVO 1
#define TERMNORM 2
#define PARADO 3
#define TERMSENAL 4

/*selectores lista_jobs*/
#define JOBS_SELECT_ALL  0
#define JOBS_SELECT_STOP 1
#define JOBS_SELECT_TERM 2
#define JOBS_SELECT_SIG  3
#define JOBS_SELECT_ACT  4

/*Comportamiento funcion recursiva.*/
#define REC_ANTES 0
#define REC_DESPUES 1
#define REC_NO 2

/*Tipos de MEM*/
#define MALLOC 0
#define MMAP 1
#define SHARED 2

/*Tipos de REDIRECCION*/
#define RE_INPUT 0
#define RE_OUTPUT 1
#define RE_ERR 2


//-----------------------------------------------------------------------------//
//----------------------------- ESTRUCTURA SIGACTION --------------------------//
//-----------------------------------------------------------------------------//
    
struct senal{
    int showhandler;
    int resendhandler;
    int sleeptimehandler;
    int counthandler;
};  

//-----------------------------------------------------------------------------//
//-----------------------FUNCIONES SEÑALES PROPORCIONADAS----------------------//
//-----------------------------------------------------------------------------//
	struct SEN{
	  char *nombre;
	  int senal;
	};
	static struct SEN sigstrnum[]={
		"HUP", SIGHUP,
		"INT", SIGINT,
		"QUIT", SIGQUIT,
		"ILL", SIGILL, 
		"TRAP", SIGTRAP,
		"ABRT", SIGABRT,
		"IOT", SIGIOT,
		"BUS", SIGBUS,
		"FPE", SIGFPE,
		"KILL", SIGKILL,
		"USR1", SIGUSR1,
		"SEGV", SIGSEGV,
		"USR2", SIGUSR2, 
		"PIPE", SIGPIPE,
		"ALRM", SIGALRM,
		"TERM", SIGTERM,
		"CHLD", SIGCHLD,
		"CONT", SIGCONT,
		"STOP", SIGSTOP,
		"TSTP", SIGTSTP, 
		"TTIN", SIGTTIN,
		"TTOU", SIGTTOU,
		"URG", SIGURG,
		"XCPU", SIGXCPU, 
		"XFSZ", SIGXFSZ,
		"VTALRM", SIGVTALRM,
		"PROF", SIGPROF,
		"WINCH", SIGWINCH, 
		"IO", SIGIO,
		"SYS", SIGSYS,
	/*senales que no hay en todas partes*/
	#ifdef SIGPOLL
		"POLL", SIGPOLL,
	#endif
	#ifdef SIGPWR
		"PWR", SIGPWR,
	#endif
	#ifdef SIGEMT
		"EMT", SIGEMT,
	#endif
	#ifdef SIGINFO
		"INFO", SIGINFO,
	#endif
	#ifdef SIGSTKFLT
		"STKFLT", SIGSTKFLT,
	#endif
	#ifdef SIGCLD
		"CLD", SIGCLD,
	#endif
	#ifdef SIGLOST
		"LOST", SIGLOST,
	#endif
	#ifdef SIGCANCEL
		"CANCEL", SIGCANCEL,
	#endif
	#ifdef SIGTHAW
		"THAW", SIGTHAW,
	#endif
	#ifdef SIGFREEZE
		"FREEZE", SIGFREEZE,
	#endif
	#ifdef SIGLWP
		"LWP", SIGLWP,
	#endif
	#ifdef SIGWAITING
		"WAITING", SIGWAITING,
	#endif
		NULL,-1,
		};    /*fin array sigstrnum */

	int Senal(char * sen)  /*devuel el numero de senial a partir del nombre*/ 
	{ 
	  int i;
	  for (i=0; sigstrnum[i].nombre!=NULL; i++)
		if (!strcmp(sen, sigstrnum[i].nombre))
			return sigstrnum[i].senal;
	  return -1;
	}


	char *NombreSenal(int sen)  /*devuelve el nombre senal a partir de la senal*/ 
	{			/* para sitios donde no hay sig2str*/
	 int i;
	  for (i=0; sigstrnum[i].nombre!=NULL; i++)
		if (sen==sigstrnum[i].senal)
			return sigstrnum[i].nombre;
	 return ("SIGUNKNOWN");
	}

	//-----------------------------------------------------------------------------//
	//------------------------------FUNCIONES COMUNES------------------------------//
	//-----------------------------------------------------------------------------//

	long int parse_num(char* pid_str, int base){
	    long long num;
	    char * endptr;
	    
	    if(pid_str == NULL){
		printf("Error parseando entero");    
		return -1;
	    }
	    
	    errno = 0; /* Ponemos errno a 0 para saber si se activó
			en la ejecucion del siguiente comando.*/
	    
	    num = strtoul(pid_str,&endptr,base);
	    
	    if (errno != 0 || *endptr != '\0'){
		perror("Error parseando entero");
		return -1;
	    }
	    
	    return num;
	}

	long int parse_hex(char* s){
	    return parse_num(s,16);
	}

	long int parse_dec(char* s){
	    return parse_num(s,10);
	}
	
	//-----------------------------------------------------------------------------//
	//------------------------------ REDIRECCIONAR --------------------------------//
	//-----------------------------------------------------------------------------//

	
	void redireccionar(const char * filename,int var){
		int fd,flags;
		
		flags = (var == RE_INPUT) ? O_RDONLY:(O_WRONLY | O_CREAT);
		
		if ((fd = open(filename,flags,S_IRWXU)) == -1){
  			perror("ERROR: No se puede redirigir.");
  			exit(0);
  		}
		close(var);
		dup(fd);
		close(fd);
	}



	//-----------------------------------------------------------------------------//
	//------------------------------------ MEM -----------------------------------//
	//-----------------------------------------------------------------------------//

	typedef struct e_mem{
		void * direccion; //Direccion de memoria en la que está asignado
		int tamanho; //Tamaño
		time_t instant; //Instante en el que se asignó la memoriav	char * fichero;
		char * fichero; //Fichero mapeado
	}e_mem;

	//Cabeceras de algunas funciones de MEM que se necesitan para las listas
	void print_element(e_mem * p,int tipo);



	//-----------------------------------------------------------------------------//
	//------------------------------------- JOBS ----------------------------------//
	//-----------------------------------------------------------------------------//

	typedef struct proceso_jobs{
	    int pid; //PID del proceso
	    time_t time; //Hora a la que se empezo a ejecutar.
	    int priority; //Prioridad
	    char* comand; //Comando ejecutado
	    
	    int estado; //Estado del proceso.
	    int valor_de_retorno; //Valor de retorno o señal causante del estado actual.
	    
	    struct rusage * uso; // Puntero a una estructura donde se almacena información de uso.
	}proceso_jobs;

	//Cabeceras de algunas funciones de jobs que se necesitan para las listas.
	int pertenece_selector(proceso_jobs * p, int selector);
	void formatear_e_imprimir_jobs(proceso_jobs * p);
	void actualizar_info_proceso(proceso_jobs * p);
	void imprimir_info_proceso(proceso_jobs * p, int selector);


	//-----------------------------------------------------------------------------//
	//----------------------------- FUNCIONES LISTA -------------------------------//
	//-----------------------------------------------------------------------------//

	typedef struct lista{
		int size;
		int n_elementos;
		void** elementos;
	} tLista;

	tLista * nuevaLista(){
		tLista * l;
		if ((l = (tLista*) malloc(sizeof(tLista))) == NULL){
		    perror("Error reservando memoria para tabla");
		    return NULL;
		}
		
		if ((l->elementos = (void**) malloc(sizeof(void*) * INI_LIST)) == NULL){
		    perror("Error reservado memoria para tabla");
		    free(l);
		    return NULL;
		}
		
		l->size = 10;
		l->n_elementos = 0;
		return l;
	}

	char* getElemento(tLista * l, int pos){
	    if(l->size <= pos) return NULL;
	    return l->elementos[pos];
	}

	int listaVacia(tLista * l){
		return (l->n_elementos == 0);
	}

	int primerElemento(tLista *l){
		if (listaVacia(l)) return -1;
		else return 0;
	}

	int siguienteElemento(tLista* l, int pos){
	    if ((l->n_elementos <= pos+1) || (pos < 0)) return -1;
	    return pos + 1;
	}

	int ultimoElemento(tLista * l, int pos){
		return (l->n_elementos == pos - 1);
	}

	int ampliarLista(tLista * l){
	    void ** nueva_lista = (void**) malloc(sizeof(void*) * (l->size*2));
		int i;
			
		if (nueva_lista == NULL){
			perror("Error reservando memoria:");
			return -1;
		}
					
		for(i = 0; i < l->size; i++) nueva_lista[i] = l->elementos[i];
		free(l->elementos);
		l->elementos = nueva_lista;
		l->size *= 2;
		return 0;
	}

	/*
		Recibe una lista y un valor a buscar y devuelve su posicion en la lista o
			-1 en caso de que no exista ese valor.
			
		NOTA: La variable direccion solo se utiliza para las listas donde se almacena la memoria asignada y
		permite escoger si a busqueda es por el campo "direccion" o por los campos habituales.
	*/
	int buscarElemento(tLista * l, int tipo, void* a,int direccion){
		
		int i;
		
		switch (tipo){
		    case LISTA_PATH:	for(i = 0; i < l->n_elementos; i++)
						if (!strcmp((char*) l->elementos[i],a)) return i;
						break;

		    case LISTA_JOBS: 	for(i=0; i<l->n_elementos; i++)
						if ( ((proceso_jobs*) l->elementos[i])->pid == ((proceso_jobs*) a)->pid) return i;
					break;

		case LISTA_SHARED:
		    case LISTA_MALLOC:	for(i=0; i<l->n_elementos; i++){
								if (direccion && ((e_mem*) l->elementos[i])->direccion == ((e_mem*) a)->direccion) return i;
									if (!direccion && ((e_mem*) l->elementos[i])->tamanho == ((e_mem*) a)->tamanho) return i;
								}
					break;	                         

		    case LISTA_MMAP:	for(i=0; i<l->n_elementos; i++){
						if (direccion && ((e_mem*) l->elementos[i])->direccion == ((e_mem*) a)->direccion) return i;
						if (!direccion && !strcmp(((e_mem*) l->elementos[i])->fichero, ((e_mem*) a)->fichero)) return i;
					}
					break;	                    
		}	
		return -1;
	}


	int insertarLista(tLista * l, int tipo, void* a){

		//No se insertan elementos que ya estan en la lista.
		if (buscarElemento(l,tipo,a,1) >= 0) return 0;
		
		//Si la lista no tiene suficiente espacio se reserva mas.
		if (l->size == l->n_elementos)
		    if (ampliarLista(l)) return -1;
		
		l->elementos[l->n_elementos] = a;
		l->n_elementos++;
		
		return 0;
	}

	int eliminarElemento(tLista * l, int tipo, int pos, int selector){
	    int eliminar = 1,i;
	    
		if ((l->n_elementos <= pos) || (pos < 0)) return -1;
				
		if (tipo == LISTA_JOBS){
		    actualizar_info_proceso((proceso_jobs *) l->elementos[pos]);
		    if (pertenece_selector((proceso_jobs *) l->elementos[pos],selector)){
			free(((proceso_jobs *) l->elementos[pos])->comand);
			free(((proceso_jobs *) l->elementos[pos])->uso);
		    }else eliminar = 0;
		}
		
		if (tipo == LISTA_MMAP)
			free(((e_mem *) l->elementos[pos])->fichero);
		
		if (tipo == LISTA_MALLOC)
			free(((e_mem *) l->elementos[pos])->direccion);

		if(eliminar){
			free(l->elementos[pos]);
		    for(i = pos; i < l->n_elementos; i++)
			    l->elementos[i] = l->elementos[i+1];
		
		    l->n_elementos--;
	    }
	    return 0;
	}

	void mostrarLista(tLista * l, int tipo,int selector){
	    int i;
	    switch(tipo){
		case LISTA_PATH: 
		    for(i=0; i<l->n_elementos; i++)
			printf("%s\n", (char*) l->elementos[i]);
		    break;
		case LISTA_JOBS:
		    for(i=0; i<l->n_elementos; i++)
			imprimir_info_proceso((proceso_jobs*) l->elementos[i],selector);
		    break;
		case LISTA_MALLOC:
		case LISTA_MMAP:
		case LISTA_SHARED:
		    for(i=0; i<l->n_elementos; i++)
					print_element((e_mem*)l->elementos[i],tipo);
		    break;
	    }
		
	}

	void clearLista(tLista * l, int tipo, int selector){
	    int i;
	    switch (tipo){ 
		case LISTA_MALLOC:
		case LISTA_MMAP:
		case LISTA_SHARED:   
		case LISTA_PATH:
		    for(i = l->n_elementos-1; i >= 0; i--) eliminarElemento(l,tipo,i,0);
		    break;
		case LISTA_JOBS:
		    for(i=l->n_elementos-1; i >= 0; i--) eliminarElemento(l,tipo,i,selector);
		    break;
	    }

	}

	//-----------------------------------------------------------------------------//
	//--------------------------- VARIABLES GLOBALES ------------------------------//
	//-----------------------------------------------------------------------------//
	tLista * lista_path;
	tLista * lista_jobs;
	tLista * lista_malloc;
	tLista * lista_mmap;
	tLista * lista_shared;
	int mode_environ;
	struct senal sigparametros[32];
	int limitresend;
	int countsigact;
	extern char **environ;

	//-----------------------------------------------------------------------------//
	//------------------------------ FUNCION SALIR --------------------------------//
	//-----------------------------------------------------------------------------//

	void eliminarLista(tLista * l, int tipo){

	    clearLista(l,tipo,0);
	    free(l->elementos);
	    free(l);

	}

	void salir(){

	    eliminarLista(lista_jobs, LISTA_JOBS);
	 
	    eliminarLista(lista_malloc, LISTA_MALLOC);
	    
	    eliminarLista(lista_mmap, LISTA_MMAP);

	    eliminarLista(lista_shared, LISTA_SHARED);

	    eliminarLista(lista_path, LISTA_PATH);

	    exit(0);
	}


	//-----------------------------------------------------------------------------//
	//-----------------------------FUNCIONES AURORES-------------------------------//
	//-----------------------------------------------------------------------------//

	void autores(){
		printf("AUTORES\nChavarría Teijeiro, Marcos. LOGIN: marcos.chavarria\nCastro Valiño, Pablo. LOGIN: pablo.castro1\n");		
	}

	//-----------------------------------------------------------------------------//
	//-------------------------------FUNCIONES PID---------------------------------//
	//-----------------------------------------------------------------------------//

	/* Imprime el PID del proceso y de su padre.*/
	void pid(){
		printf("Hijo: %i  Padre: %i \n",getpid(),getppid());
	}

	//-----------------------------------------------------------------------------//
	//-------------------------------FUNCION PWD-----------------------------------//
	//-----------------------------------------------------------------------------//

	/*Imprime el directorio actual del shell*/
	void pwd(){

		char pwd[PATH_MAX];
		
		if (getcwd(pwd,PATH_MAX) != NULL)
		printf("%s\n", pwd);
	    else
		perror("Error mostrando el directorio actual");
	}

	//-----------------------------------------------------------------------------//
	//-------------------------------FUNCIONES PATH--------------------------------//
	//-----------------------------------------------------------------------------//

	char* which(char* file){
	    
	    struct stat s;
	    int pos;
	    static char archivo[PATH_MAX];

	    for(pos=primerElemento(lista_path); pos != -1 ; pos=siguienteElemento(lista_path,pos)){
		strcpy(archivo, getElemento(lista_path,pos));
		strcat(archivo,"/");
		strcat(archivo,file);
		if (stat(archivo,&s) == 0) return archivo; //Comprueba si existe el archivo.
	    }
	    
	    return NULL;   
	}


	void path_add_envpath(){
	    char* elemento, *env_path, *mod_path, *a;

	    env_path = getenv("PATH");

	    mod_path = (char*) malloc(sizeof(char) * (strlen(env_path) + 1));
	    strcpy(mod_path,env_path);
	    
	    elemento = strtok(mod_path,":");

	    while(elemento != NULL){
		a = (char*)  malloc(sizeof(char) *(strlen(elemento) + 1));
		insertarLista(lista_path, LISTA_PATH,strcpy(a,elemento));
		elemento = strtok(NULL,":");
	    }
	    free(mod_path);
	}

	void path_add( char * dir){
	  if (dir==NULL)
			mostrarLista(lista_path,LISTA_PATH,0);
	    else{
		char * a =  malloc(sizeof(char) *(strlen(dir) + 1));
	  
		if (insertarLista(lista_path, LISTA_PATH, strcpy(a,dir))==-1)
			    printf ("Imposible anadir al path");
	    }
	}

	void path_del(char* dir){
	    int pos;
	    if (dir==NULL)
		mostrarLista(lista_path,LISTA_PATH,0);
	    /* Buscamos la posicion de de un elemento y lo eliminamos. Si se produce un error lo mostramos.*/
	    else if (((pos = buscarElemento(lista_path,LISTA_PATH,dir,0)) == -1) || (eliminarElemento(lista_path,LISTA_PATH,pos,0) == -1))
		printf("No existe el elemento \"%s\" en el path.", dir);
	}

	void path_find(char* dir){
	    if (dir==NULL)
		mostrarLista(lista_path,LISTA_PATH,0);
	    else{
		char * p = which(dir);
		printf("%s\n",p==NULL ? "Programa no encontrado." : p);
	    }
	}

	void path(char * opciones[]){
	    if (opciones[0]==NULL)
		    mostrarLista(lista_path,LISTA_PATH,0);
	    else if  (!strcmp(opciones[0],"-list"))
		    mostrarLista(lista_path,LISTA_PATH,0);
	    else if  (!strcmp(opciones[0],"-add"))
		    path_add(opciones[1]);
	    else if  (!strcmp(opciones[0],"-del"))
		    path_del(opciones[1]);
	    else if (!strcmp(opciones[0],"-clear"))
		clearLista(lista_path,LISTA_PATH,0);
	    else if (!strcmp(opciones[0],"-path"))
		path_add_envpath();  
	    else if (!strcmp(opciones[0],"-find"))
		path_find(opciones[1]);
	    else 
		printf("Usage path [-add|-list|-del|-clear|-path|-find] [arg]\n");
	}  

	//-----------------------------------------------------------------------------//
	//-------------------------FUNCION CHANGE DIRECTORY----------------------------//
	//-----------------------------------------------------------------------------//

	void change_directory(char * opciones[]){
	    if (opciones[0] == NULL) 
		pwd();
	    else if (chdir(opciones[0]))
		perror("CD");
	}

	//-----------------------------------------------------------------------------//
	//-----------------------------FUNCIONES ENVIRON-------------------------------//
	//-----------------------------------------------------------------------------//


	/*
	Funcion que busca "value" en "arg" y devuelve la posición, en caso de 
	no encontrarlo devuelve -1
	*/
	int search_pos(char * arg[],char * value){
	    int i;
	       
	    for(i=0; arg[i] != NULL; i++) if (!strncmp(arg[i],value,strlen(value))) return i;
		return -1;
	}


	/*
	Funcion que sustituye la variable indicada en old_name por la new_name
	en funcion del modo en el que esté.
	*/
	void sust(char * old_name, char* new_name,char *env[],char **environ){
		int pos = -1;

		//Si el modo es MAIN
		if (mode_environ == T_MAIN){
		    if ((pos = search_pos(env,old_name)) != -1)
				env[pos] = new_name;
		}
			
		if (mode_environ == T_ENVIRON){
			if ((pos = search_pos(environ,old_name)) != -1)
				environ[pos] = new_name;		
		}
		
		if (pos == -1) printf("imposible cambiar variable %s  (No such file or directory)\n",old_name);
		
	}

	/*--------OPCIONES DE ENVIRON--------*/

	/*
	OPCION MOSTRAR ENTORNO
	Funcion que muestra el entorno del proceso en función del modo
	en el que esté.
	*/
	void mostrar_entorno(char *	env[]){
		int i;
		//Si el modo es ENVIRON
		if (mode_environ == T_ENVIRON){
		for(i=0; environ[i] != NULL; i++)
				printf ("%p-->environ: %s(%p)\n", &environ[i],environ[i],environ[i]);
		}
		//Si el modo es el 3º argumento de MAIN
		if (mode_environ == T_MAIN){
			for(i=0; env[i] != NULL; i++)
				printf ("%p-->tercer arg main: %s(%p)\n", &env[i],env[i],env[i]);
		}
	}

	/*
	OPCION -MODE
	*/
	void opcion_mode(char ** argumentos){
		if (argumentos[1] == NULL) printf("MODO DE ACCESO: %s\n", mode_environ == T_ENVIRON ? "ENVIRON" : "MAIN" );
		else if (!strcmp(argumentos[1], "main")) mode_environ = T_MAIN;
		else if (!strcmp(argumentos[1], "environ")) mode_environ = T_ENVIRON;
		else printf("Uso: environ -mode [environ|main]\n");
	}

	/*
	OPCION -SUS
	*/
	void opcion_sus(char ** argumentos,char *env[]){

		if ((argumentos[1] != NULL) && (argumentos[2] != NULL)){
		    char * old_name = strdup(argumentos[1]);
			char * new_name = strdup(argumentos[2]);
			sust(old_name,new_name,env,environ);
			free(old_name);
		}else printf("uso: environ -sus oldvar var=nuevovalor\n");
	}

	/*
	OPCION -PUTENV
	*/
	void opcion_putenv(char ** argumentos){
		int i;
	    char * new_entry;
		
	    for(i = 1; argumentos[i] != NULL; i++){
			new_entry = strdup(argumentos[i]);		
			if (putenv(new_entry)) 
				perror("PUTENV, la variable no se ha podido añadir");
		}
	}

	/*
	OPCION -ADDR
	*/
	void opcion_addr(char *env[]){
		printf ("%p--> env(%p)\n", &env,env);
		printf ("%p--> environ(%p)\n", &environ,environ);
	}


	/*
	OPCION IMPRIMIR VARIABLE ENTORNO
	Funcion que se le pasa una variable y la imprime de las 3 formas distintas
	*/
	void imprimir_var_entorno(char * env[],char * value){
		
		int pos = -1;
		char * p;
		//Imprime la variable en el 3º argumento de main
		pos = search_pos(env,value);
		if (pos != (-1)) printf ("Mediante arg Main: %p--> %s(%p)\n", &env[pos],env[pos],env[pos]);	
		
		//Imprime la variable en environ
		pos = search_pos(environ,value);
		if (pos != (-1)) printf ("Mediante environ: %p--> %s(%p)\n", &environ[pos],environ[pos],environ[pos]);
		
		//Imprime la variable con getenv
		if ((p=getenv(value)) !=NULL) printf("Mediante getenv: %s (%p)\n",p,p);
		else printf("Error: La variable %s no existe\n",value);
	}

	/*
	OPCION SUSTITUIR VARIABLE ENTORNO
	*/
	void sustituir_var_entorno(char *env[],char * value){

		char * new_name = strdup(value);
		char * old_name = strdup(strtok(value,"="));
		
	    sust(old_name,new_name,env,environ);

		free(old_name);
	}

	/*
	Funcion que comprueba si hay un igual en el argumento introducido
	*/
	int comprueba_igual(char * argumento){
		int i;
		//Busca el "=" para saber si tiene que imprimir las variables o modificarlas
		for(i = 0; i <= strlen(argumento); i++)	if(argumento[i] == '=') return 1;
		return 0;
	}

	/*
	Funcion que recorre todos los argumentos escogiendo si hay que sustituir 
	o hay que imprimir una variable del entorno
	*/
	void recorre_opciones(char ** opciones, char *env[]){
		int i;
	    for(i=0; opciones[i] != NULL; i++){
			if (comprueba_igual(opciones[i])) 
			    sustituir_var_entorno(env,opciones[i]);
			else 
			    imprimir_var_entorno(env,opciones[i]);
		}

	}

	/*
	FUNCION PRINCIPAL DE ENVIRON
	*/

	void environ_f(char ** argumentos,char *env[]){


		//Para mostrar el Entorno
		if (argumentos[0] == NULL) mostrar_entorno(env);
		else if (!strcmp(argumentos[0], "-mode"))   opcion_mode(argumentos); //Opcion: MODE
	    else if (!strcmp(argumentos[0], "-sus"))    opcion_sus(argumentos,env);	//Opcion: SUS
		else if (!strcmp(argumentos[0], "-putenv")) opcion_putenv(argumentos); //Opcion: PUTENV
		else if (!strcmp(argumentos[0], "-addr"))   opcion_addr(env); //Opcion: ADDR
		else recorre_opciones(argumentos,env);
	}


	//-----------------------------------------------------------------------------//
	//--------------------------------FUNCION FORK---------------------------------//
	//-----------------------------------------------------------------------------//

	void shell_fork(){
	    int pid;
	    
	    if ((pid = fork()) <0){
		perror("FORK: Error en la creación del proceso.");
		return;
	    }
	    
	    if (pid){ //PID != 0 -->PADRE
		printf("Fork de la shell con PID %i iniciado.\n",pid);
		wait(NULL);
		printf("Fork de la shell con PID %i terminado.\n",pid);
	    }
	}


	//-----------------------------------------------------------------------------//
	//----------------------------FUNCIONES PRIORIDAD------------------------------//
	//-----------------------------------------------------------------------------//

	void mostrar_prioridad(int pid){
	    int prioridad,pol; 
	    struct sched_param  param;
	    errno = 0;
	    if ((prioridad = getpriority(PRIO_PROCESS, pid))==-1 && errno)
		perror ("imposible obtener prioridad con getpriority") ;
	    else
	      printf ("prioridad de get priority de proceso %d: %d\n",pid,prioridad);
	    
	    pol=sched_getscheduler(pid);
	    sched_getparam(pid,&param);
	    prioridad=param.sched_priority;
	    switch(pol){
		case SCHED_OTHER: printf("Proceso %i -> %i OTHER\n",pid,prioridad); break;
		case SCHED_BATCH: printf("Proceso %i -> %i BATCH\n",pid,prioridad); break;
		case SCHED_IDLE: printf("Proceso %i -> %i IDLE\n",pid,prioridad); break;
		case SCHED_FIFO: printf("Proceso %i -> %i FIFO\n",pid,prioridad); break;
		case SCHED_RR:  printf("Proceso %i -> %i Round Robin\n",pid,prioridad); break;
		case -1: perror("Error obteniendo la politica de planificacion"); return;
		default: printf("Error, politica de planificacion no conocida.\n");
	    }
	    
	}

	void priority(char ** opciones){
	    int pid=getpid(), pri;
	    
	    if (opciones[0] == NULL) {
		mostrar_prioridad(pid);
		return;
		}
	    pid=atoi(opciones[0]);
	    if (opciones[1] == NULL){
		mostrar_prioridad(pid);
		return;
		}
	    if((pri=(pid_t) parse_dec(opciones[1])) == -1 && errno) return;
	    if (setpriority(PRIO_PROCESS, pid, pri)==-1)
		perror ("Imposible cambiar prioridad.");
		
	}
	int get_politica(char* politica){

	    if (!strcmp(politica,"OTHER")) return SCHED_OTHER;
	    else if (!strcmp(politica,"BATCH")) return SCHED_BATCH;
	    else if (!strcmp(politica,"IDLE")) return SCHED_IDLE;
	    else if (!strcmp(politica,"RR")) return SCHED_RR;
	    else if (!strcmp(politica,"FIFO")) return SCHED_FIFO;
	    else return -1;
	}

	void posixpri(char** opciones){
	    int politica, pid;
	    struct sched_param  param;

	    if (opciones[0] == NULL || opciones[1] == NULL){
		printf("Usage:posixpri pol pri [pid]\n");
		return;    
	    }else if ((politica = get_politica(opciones[0])) < 0){
		printf("Error: prioridad no reconocida.\n");
		return;
	    }else if ((param.sched_priority = parse_dec(opciones[1])) < 0){
		return;
	    }
	    
	    if ((pid = opciones[2] == NULL ? getpid() : parse_dec(opciones[2])) < 0) return;
	    
	    if (sched_setscheduler(pid, politica, &param) == -1) perror("Error cambiando prioridad");
	}

	//-----------------------------------------------------------------------------//
	//-------------------------------FUNCIONES JOBS--------------------------------//
	//-----------------------------------------------------------------------------//

	int pertenece_selector(proceso_jobs * p, int selector){

	   if (selector == JOBS_SELECT_ALL) return 1;
	   else if (selector == JOBS_SELECT_STOP) return p->estado == PARADO;
	   else if (selector == JOBS_SELECT_TERM) return p->estado == TERMNORM;
	   else if (selector == JOBS_SELECT_SIG) return  p->estado == TERMSENAL;
	   else if (selector == JOBS_SELECT_ACT) return  p->estado == ACTIVO;
	   else return 0;
	}

	void formatear_e_imprimir_jobs(proceso_jobs * p){
	    char estado[24], * fecha;
	    int i;
	    
	    fecha = (char*) ctime(&(p->time));
	    for(i=0; fecha[i] != '\n' && fecha[i] != '\0'; i++);
	    fecha[i] = '\0'; // Se elimina el salto de linea que devuelve ctime al final.
	    
	    if (p->estado == ACTIVO){
		printf(" %4i %11s (\?\?\?) %2i %s %s\n",p->pid,"ACTIVO",p->priority,fecha, p->comand);   
	    }else{
		if (p->estado == PARADO) sprintf(estado,"PARADO");
		else if (p->estado == TERMNORM) sprintf(estado,"TERM NORMAL");
		else if (p->estado == TERMSENAL) sprintf(estado,"TERM SENAL");
	       
		printf(" %4i %11s (%3i) \?\? %s %s\n",p->pid,estado, p->valor_de_retorno,fecha, p->comand);   
	    }
	}

	void actualizar_info_proceso(proceso_jobs * p){
	    int status;
	    
	    /*Si los procesos ya estan en un estado de terminado no hay nada que actualizar.*/
	    if (p->estado == TERMNORM || p->estado == TERMSENAL) return;
	    
	    if (p->uso == NULL)
		p->uso = (struct rusage *) malloc(sizeof(struct rusage));

	    p->priority = getpriority(PRIO_PROCESS,p->pid);
	    
	    if(wait4(p->pid, &status,WUNTRACED | WNOHANG | WCONTINUED , p->uso) > 0){
		if (WIFEXITED(status)){
		    p->valor_de_retorno = WEXITSTATUS(status);
		    p->estado = TERMNORM;
		}else if (WIFSIGNALED(status)){
		    p->valor_de_retorno = WTERMSIG(status);
		    p->estado = TERMSENAL;
		}else if (WIFSTOPPED(status)){
		    p->valor_de_retorno= WSTOPSIG(status);
		    p->estado = PARADO;
		}else if (WIFCONTINUED(status))
		    p->estado = ACTIVO;
	    }
	}

	void imprimir_info_proceso(proceso_jobs * p, int selector){

	    actualizar_info_proceso(p);
	    
	    if (pertenece_selector(p,selector)){
		formatear_e_imprimir_jobs(p);
	    }
	}

	char * reconstruir_comando(char ** argv, char** arge, int environ){
	    int i, size = 0;
	    char * cmd;
	    
	    //Se comprueba el tamaño que hay que reservar.
	    for(i=0; argv[i] != NULL; i++) size += strlen(argv[i])+1;
	    if (environ){ 
		size += 3;
		for(i=0; arge[i] != NULL; i++) size += strlen(arge[i])+1;
	    }
	    cmd = (char*) malloc(sizeof(char) * (size + 1));
	    
	    strcpy(cmd,argv[0]);
	    for(i=1; argv[i] != NULL; i++){
		strcat(cmd," ");
		strcat(cmd,argv[i]);
	    }
	    
	    if (environ){
		strcat(cmd, " **");
		for(i=0; arge[i] != NULL; i++){
		    strcat(cmd," ");
		    strcat(cmd,arge[i]);
		}
	    }
	    return cmd;
	}

	int insertar_trabajo(char ** argv, char ** arge, int environ, int pid){
	    proceso_jobs * elemento = (proceso_jobs*) malloc(sizeof(proceso_jobs));
	    
	    if (elemento == NULL){
		perror("Error reservando memoria para trabajo");
		return -1;
	    }
	    
	    elemento->pid = pid;
	    elemento->time = time(NULL);
	    elemento->comand = reconstruir_comando(argv,arge,environ);
	    elemento->estado = ACTIVO;
	    elemento->uso = NULL;
	    
	    actualizar_info_proceso(elemento);
	    
	    return insertarLista(lista_jobs, LISTA_JOBS, elemento);
	}

	void imprimir_uso_proceso(struct rusage * r){
	    printf("User CPU time used: %li s %li us\n",r->ru_utime.tv_sec,r->ru_utime.tv_usec); 
	    printf("System CPU time used: %li s %li us\n",r->ru_stime.tv_sec,r->ru_stime.tv_usec);
	    printf("Maximum resident set size %li\n",r->ru_maxrss);
	    printf("Integral shared memory size %li\n",r->ru_ixrss);
	    printf("Integral unshared data size %li\n",r->ru_idrss);
	    printf("Integral unshared stack size %li\n",r->ru_isrss);
	    printf("Page reclaims %li\n",r->ru_minflt);
	    printf("Page faults %li\n",r->ru_majflt);
	    printf("Swaps %li\n",r->ru_nswap);
	    printf("Block input operations %li\n",r->ru_inblock);
	    printf("Block output operations %li\n",r->ru_oublock);
	    printf("Messages sent %li\n",r->ru_msgsnd);
	    printf("Messages received %li\n",r->ru_msgrcv);
	    printf("Signals received %li\n",r->ru_nsignals);
	    printf("Voluntary context switches %li\n",r->ru_nvcsw);
	    printf("Involuntary context switches %li\n",r->ru_nivcsw);
	}


	void jobs_pid(char * pid_str){
	    int pos;
	    proceso_jobs p, *elemento;
	    
	    //Parsemos el PID pasado por linea de comandos y creamos una elemento de la lista vacio con ese PID.
	    if((p.pid = parse_dec(pid_str)) == -1) return;
	    
	    if ((pos = buscarElemento(lista_jobs,LISTA_JOBS,&p,0)) == -1){
		printf("Error: No existe proceso con ese pid.\n");
	    }else{
		elemento = (proceso_jobs*) getElemento(lista_jobs,pos);
		imprimir_info_proceso(elemento,0);
		if(elemento->estado == TERMNORM) imprimir_uso_proceso(elemento->uso);  
	    } 
	}

	void jobs_del(char ** opciones){
	    if (opciones == NULL)                 clearLista(lista_jobs, LISTA_JOBS, JOBS_SELECT_ALL);
	    else if (!strcmp(opciones[0],"all"))  clearLista(lista_jobs, LISTA_JOBS, JOBS_SELECT_ALL);
	    else if (!strcmp(opciones[0],"term")) clearLista(lista_jobs, LISTA_JOBS, JOBS_SELECT_TERM);
	    else if (!strcmp(opciones[0],"sig"))  clearLista(lista_jobs, LISTA_JOBS, JOBS_SELECT_SIG);
	    else if (!strcmp(opciones[0],"stop")) clearLista(lista_jobs, LISTA_JOBS, JOBS_SELECT_STOP);
	    else if (!strcmp(opciones[0],"act"))  clearLista(lista_jobs, LISTA_JOBS, JOBS_SELECT_ACT);
	    else printf("Usage: jobs [-del] [all|act|term|sig|stop]\n");
	}

	void jobs(char * opciones[]){
	    
	    if (opciones[0] == NULL)              mostrarLista(lista_jobs, LISTA_JOBS, JOBS_SELECT_ALL);
	    else if (!strcmp(opciones[0],"all"))  mostrarLista(lista_jobs, LISTA_JOBS, JOBS_SELECT_ALL);
	    else if (!strcmp(opciones[0],"term")) mostrarLista(lista_jobs, LISTA_JOBS, JOBS_SELECT_TERM);
	    else if (!strcmp(opciones[0],"sig"))  mostrarLista(lista_jobs, LISTA_JOBS, JOBS_SELECT_SIG);
	    else if (!strcmp(opciones[0],"stop")) mostrarLista(lista_jobs, LISTA_JOBS, JOBS_SELECT_STOP);
	    else if (!strcmp(opciones[0],"act"))  mostrarLista(lista_jobs, LISTA_JOBS, JOBS_SELECT_ACT);
	    else if (!strcmp(opciones[0],"-del")) jobs_del(opciones+1);
	    else jobs_pid(opciones[0]); 
	}

	//-----------------------------------------------------------------------------//
	//------------------ FUNCIONES EXECUTE, BACKGROUND Y PROG ---------------------//
	//-----------------------------------------------------------------------------//

	int starts_with(char* src, char* beg){
	    if (strlen(src) < strlen(beg)) return 0;
	    return !strncmp(src,beg,strlen(beg));
	}

	int es_path_absoluto(char* path){
	    return starts_with(path,"./") || starts_with(path,"../") || starts_with(path,"/"); 
	}

	char ** construir_entorno (char ** opciones){
	    static char * entorno[MAXCOMANDO/2];
	    char * igual;
	    extern char ** environ;
	    int i, j=0, pos;
	    
	    for(i=0; opciones[i] != NULL; i++){
		igual = strchr(opciones[i],'=');
		if (igual == NULL){
		    if ((pos = search_pos(environ,opciones[i])) != -1)
			entorno[j++] = environ[pos];
		}else
		    entorno[j++] = opciones[i];
	    }
	    
	    return entorno;
	}

	int extraer_params(char ** opciones, char ** programa, char *** argv, char *** arge, int * environ,
		int * priority, int * priorityval,char ** redir_in, char ** redir_out, char ** redir_err){
		
		int i,j,in=-1,out=-1,err =-1;
		
	    *programa = es_path_absoluto(opciones[0]) ? opciones[0] : which(opciones[0]);
	    if (*programa == NULL){printf("Error: Programa no encontrado.\n"); return -1;}

	    for(i=0;opciones[i] != NULL && opciones[i][0] != '@' && strcmp(opciones[i],"**");i++){
		    if (!strncmp(opciones[i],"in:F1",3)){
				in = i;
				*redir_in = opciones[i]+3;
			}
		    if (!strncmp(opciones[i],"out:F2",4)){
				out = i;
				*redir_out = opciones[i]+4;
			}
			if (!strncmp(opciones[i],"err:F3",4)){
				err = i;
				*redir_err = opciones[i]+4;
			}
		}
		
	    if(opciones[i] != NULL && opciones[i][0] == '@'){
		*priority = 1;
		if ((*priorityval = parse_dec(opciones[i] + 1)) < 0) return -1;
		opciones[i++] = NULL;
	    }
	    
	    *environ = (opciones [i] != NULL) && !strcmp(opciones[i],"**");
	    *arge = *environ ? construir_entorno(opciones+1) : NULL;
	    opciones[i] = NULL;
	    
	    i = 0; j= 0;
		while(opciones[j] != NULL){
	    	if (j == in || j == out || j == err){//Si es una opcion de E/S se elimina
	    		opciones[j] = NULL;
		    	j++;
		    }
			else{
		    	opciones[i] = opciones[j];//Si no, se intercambia a la ultima posicion en la que hay valores utiles
				if (i != j) opciones[j] = NULL;//Si i != j se borra la posicion j
				j++;
				i++;

			}
		}
		*argv = opciones;
	    return 0;
	}

	void ejecutar(char * prog, char ** argv, char ** arge, int environ, int priority, 
		      int priorityval, int crear_proceso, int segundo_plano,char * redir_in, char * redir_out, char * redir_err){
	    int pid;
	    
	    switch(pid = crear_proceso ? fork() : 0){
			case 0:
				if(redir_in != NULL)
					redireccionar(redir_in,RE_INPUT);//Redireccionar entrada
				if(redir_out != NULL)
					redireccionar(redir_out,RE_OUTPUT);//Redireccionar salida
				if(redir_err != NULL)
					redireccionar(redir_err,RE_ERR);//Redireccionar error
				
				if (priority && setpriority(PRIO_PROCESS, getpid(), priorityval) == -1){
					perror ("imposible cambiar prioridad");
					exit(0);
				}
				if (environ){
					if (execve(prog,argv,arge) < 0) perror("ERROR");
				}else{
				   if (execv(prog,argv) < 0) perror("ERROR");
				}
				
			case -1:
				perror("ERROR");
			default:
				if (segundo_plano) insertar_trabajo(argv,arge,environ,pid);
				else wait(NULL);
	    }
	}

	void ejecutar_programa_externo(char ** opciones, int crear_proceso, int segundo_plano){
	    
	    char ** argv, ** arge, * programa, * redir_in, * redir_out, * redir_err;
	    int environ, priority,priorityval;
	    if (extraer_params(opciones, &programa, &argv,&arge,&environ,&priority,&priorityval,&redir_in,&redir_out,&redir_err)) return;
	    ejecutar(programa,argv,arge,environ,priority,priorityval,crear_proceso,segundo_plano,redir_in,redir_out,redir_err);
	}

	void execute(char **opciones){
	    if(opciones[0] == NULL)
		printf("Usage: execute prog [args] [** env]");
	    else 
		ejecutar_programa_externo(opciones,0,0);
	}

	void background(char **opciones){
	    if(opciones[0] == NULL)
		printf("Usage: background prog [args] [** env]");
	    else 
		ejecutar_programa_externo(opciones,1,1);
	}

	//-----------------------------------------------------------------------------//
//--------------------------------FUNCIONES MEM--------------------------------//
//-----------------------------------------------------------------------------//

//Funcion que parsea el tipo
char * parsea_tipo(int type){
	switch (type){
		case LISTA_MALLOC: return "MALLOC";
		case LISTA_MMAP: return "MMAP";
		case LISTA_SHARED: return "SHARED";
	}
}

//Imprime elemento de la lista de direcciones asignadas con mmap
void print_element(e_mem * p,int tipo){
	int i;
	char * fecha = (char*) ctime(&(p->instant));
	for(i=0; fecha[i] != '\n' && fecha[i] != '\0'; i++);
	fecha[i] = '\0'; // Se elimina el salto de linea que devuelve ctime al final
	if (p->fichero != NULL) printf("(%s): %s -- (%p) Tamaño: %i Instante de asignacion: %s\n",parsea_tipo(tipo),p->fichero,p->direccion,p->tamanho,fecha);	
	else printf("(%s): -- (%p) Tamaño: %i Instante de asignacion: %s\n",parsea_tipo(tipo),p->direccion,p->tamanho,fecha);
}

//Funcion que crea e inicializa un e_mmap
int inserta_e_mem(tLista * lista,int type, int tam,void * reserva,char * archivo){
  	
  	e_mem * elemento = (e_mem*) malloc(sizeof(e_mem));
   	if (elemento == NULL){
    	perror("Error reservando memoria para trabajo");
		return -1;
    }
	elemento->fichero = archivo;
	elemento->direccion = reserva;
	elemento->tamanho = tam;
	elemento->instant = time(NULL);

	insertarLista(lista,type,elemento);
	print_element(elemento,	type);
	return 0;
}


/*----------------------------------
Opcion MOSTRAR MALLOC
*/
void mostrar_malloc(){
	mostrarLista(lista_malloc, LISTA_MALLOC,0);
}

/*----------------------------------
Opcion MOSTRAR MMAP
*/
void mostrar_mmap(){
	mostrarLista(lista_mmap, LISTA_MMAP,0);
}

/*----------------------------------
Opcion MOSTRAR SHARED
*/
void mostrar_shared(){
	mostrarLista(lista_shared, LISTA_SHARED,0);
}

/*-----------------------------------
Opcion MOSTRAR TODAS LAS LISTAS
*/
void mostrar_mem(){
	mostrar_malloc();
	mostrar_mmap();
	mostrar_shared();
}


//Extrae el flag en mmap
int extrae_flag(char * flag){
	if (flag == NULL) { //comprueba que tiene tipo de mapeo
		printf("No ha introducido tipo de mapeo\n"); 
		return -1;
	}	
	if (!strcmp(flag,"private")) return MAP_SHARED;
	else if (!strcmp(flag,"shared")) return MAP_PRIVATE;
	else{
		printf("El tipo de mapeo introducido es incorrecto.\n");
		return -1;
	}
}

//Extrae los permisos en mmap
int extrae_permisos(char * permisos){
	if (permisos == NULL){ //comprueba que tiene permisos
		printf("No ha introducido correctamente los permisos de mapeo\n"); 
		return -1;
	}
	int lectura = 0,escritura = 0,ejecucion = 0,i;
	for (i=0;i<=strlen(permisos);i++){
    	if ((permisos[i] == 'r')) lectura = PROT_READ;
    	if ((permisos[i] == 'w')) escritura = PROT_WRITE;
    	if ((permisos[i] == 'x')) ejecucion = PROT_EXEC;		    
	}
	
	if (lectura != 0 || escritura != 0 || ejecucion != 0) return (lectura|escritura|ejecucion);
	return PROT_NONE;
}


/*
MALLOC: Asigna una posicion de memoria a traves de malloc
*/
int asigna_malloc(char * tamanho){
	int tam;
	//Si no hay tamaño imprime la lista de asignaciones hechas con malloc
	if (tamanho == NULL) mostrar_malloc();
	else {
	    if ((tam = parse_dec(tamanho)) < 0) return -1;
		void *reserva = malloc(tam);
		
		return (inserta_e_mem(lista_malloc,LISTA_MALLOC,tam,reserva,NULL));
	}
}

/*
MMAP: Mapea un archivo mediante mmap
*/
int mapea_mmap(char ** opciones){
	int tam = 0, flag = -1, permisos = 0, fd = -1,statdest = -1,offset = 0;
	void* map;
	char *filename;
	struct stat estructura;

	if (opciones[0] == NULL) mostrar_mmap(); //muestra dir donde estan mapeados ficheros
	else {
   		if ((fd = open(opciones[0],O_RDWR))== -1) { //extrae el file_descriptor
			perror("Error abriendo el archivo para lectura");
			return -1;
   		}
		if ((statdest = stat(opciones[0],&estructura)) < 0) { //comprobamos que stat no diese ningun error
			perror("Accediendo al archivo.");
			return -1;
		}
		if ((flag = extrae_flag(opciones[1])) == -1)//extrae el tipo y comprueba si es correcto 
			return -1; 
		if ((permisos = extrae_permisos(opciones[2])) == -1)//extrae los permisos
			return -1;
			
		tam = estructura.st_size; //extrae el tamaño
		filename = strdup(opciones[0]); //nombre de archivo
		
	    if ((map = mmap(NULL,tam,permisos,flag,fd,offset)) == MAP_FAILED) { //Realiza el mapeo y comprueba errores
			close(fd);
			perror("Error mapeando el archivo");
			return -1;
	    }
		if (inserta_e_mem(lista_mmap,LISTA_MMAP,tam,map,filename) == -1){//inserta en la lista
			close(fd);
			return -1; 
		}
		close(fd);
		return 0;
	}
}

/*
SHARED: Obtiene la memoria compartida con la clave indicada
*/
int mapea_shared(char ** opciones){
    struct shmid_ds s;
	int key, tam = 0,flags;
	key_t shmid;
	void * reserva;

	if (opciones[0] == NULL) mostrar_shared();
	else {	
		if ((key = parse_dec(opciones[0])) < 0) return -1; //extrae key
		
		if(opciones[1] != NULL && ((tam = parse_dec(opciones[1])) < 0)) //extrae tamaño si lo hay
			return -1;
			
		flags = (opciones[1] != NULL) ? (IPC_CREAT | IPC_EXCL | S_IRWXU):0;//Selecciona los flags
		
		if ((shmid = shmget(key,tam,flags)) < 0){//obtiene la memoria compartida
			perror("Error obteniendo segmento de memoria compartida.");
			return -1;
		}
		
		if(opciones[1] == NULL){
		    if (shmctl(shmid,IPC_STAT,&s) == -1){
		        perror("Error obteniendo datos de zona de memoria compartida");
		        return -1;
		    }else{
		        tam = s.shm_segsz;
		    }
		} 
		
		if ((reserva = (int*) shmat(shmid,NULL,0)) == (void *) -1) {//realiza el mapeo
			perror("Error mapeando el segmento de memoria compartida.");
			return -1;
		}
		return inserta_e_mem(lista_shared,LISTA_SHARED,tam,reserva,NULL);
	}
}

/*---------------------------------------------------
Opcion ASSIGN
*/
void opcion_assign(char ** opciones){
	if (opciones[0] == NULL) printf("Argumentos insuficientes para la opcion ASSIGN\n");
	else if(!strcmp(opciones[0], "malloc")) asigna_malloc(opciones[1]);
	else if(!strcmp(opciones[0], "mmap")) mapea_mmap(opciones+1);
	else if(!strcmp(opciones[0], "shared")) mapea_shared(opciones+1);
	else printf("La opcion introducida no es correcta.\n");
}
	
/*
DESASSIGN MALLOC
*/
int desasigna_malloc(char ** opciones,int dir){
	if (opciones[0] == NULL) mostrar_malloc();//imprime asignaciones con malloc
	else{
		int pos,direccion,tam;
   		e_mem p;
   		e_mem * elemento = malloc(sizeof(e_mem));

		if (dir){
			if ((direccion = parse_hex(opciones[0])) == -1) return -1;//Extrae la dirección y comprueba si hay error
   			p.direccion = (void *) direccion;
   	} else {
			if ((tam = parse_dec(opciones[0])) == -1) return -1;//Extrae el tamaño y comprueba si hay error
			p.tamanho = tam;
		}    	
		
		if ((pos = buscarElemento(lista_malloc,LISTA_MALLOC,&p,dir)) == -1){//Busca elemento en la lista
			if (!dir) printf("No hay ninguna asignación del tamaño especificado\n");
			return -1;
		}
		
		elemento = (e_mem *) getElemento(lista_malloc,pos);//Obtiene el elemento
		eliminarElemento(lista_malloc,LISTA_MALLOC,pos,0);//elimina el elemento de la lista
		printf("Se ha desasignado correctamente el segmento.\n");
		return 0;
	}
}

/*
DESASSIGN MMAP
*/
int desmapea_mmap(char ** opciones,int dir){
	if (opciones[0] == NULL) mostrar_mmap();//imprime asignaciones con mmap
	else{
		char * filename = strdup(opciones[0]);
		int pos, direccion;
   		e_mem p;
   		e_mem * elemento = malloc(sizeof(e_mem));

		if (dir){
			if ((direccion = parse_hex(opciones[0])) == -1) return -1;//Extrae la dirección y comprueba si hay error
   			p.direccion = (void *) direccion;
   		} else p.fichero = filename;
    	
    	if ((pos = buscarElemento(lista_mmap,LISTA_MMAP,&p,dir)) == -1){//Busca elemento en la lista
    		if (!dir) printf("No hay ninguna asignación del fichero especificado\n");
    		return -1;
    	}
		elemento = (e_mem *) getElemento(lista_mmap,pos);//Obtiene el elemento
		munmap(elemento->direccion,elemento->tamanho); //libera memoria
		free(filename);
		eliminarElemento(lista_mmap,LISTA_MMAP,pos,0);//elimina el elemento de la lista	
		printf("Se ha desasignado correctamente el segmento.\n");
		return 0;	
	}
}

/*
DESASSIGN SHARED
*/
int desmapea_shared(char ** opciones,int dir){
	if (opciones[0] == NULL) mostrar_shared();//imprime asignaciones con shared
	else{	
		int tam, pos, direccion;
		e_mem p;
	  	e_mem * elemento = (e_mem*) malloc(sizeof(e_mem));

		if (dir){
			if ((direccion = parse_hex(opciones[0])) == -1) return -1;//Extrae la dirección y comprueba si hay error
   			p.direccion = (void *) direccion;
   		}
		else {
			if ((tam = parse_dec(opciones[0])) == -1) return -1;//Extrae el tamaño y comprueba si hay error
			p.tamanho = tam;
		}
		
		if ((pos = buscarElemento(lista_shared, LISTA_SHARED,&p,dir)) == -1){//Busca el elemento en la lista
		 	if (!dir) printf("No hay ninguna asignación del tamaño especificado.\n");
    		return -1;
    	}
		elemento = (e_mem *) getElemento(lista_shared,pos);//Obtiene el elemento

		if ((shmdt(elemento->direccion)) < 0){ //libera memoria
			perror("Error liberando el segmento de memoria compartida.");
			return -1;
		}		
		eliminarElemento(lista_shared,LISTA_SHARED,pos,0);//elimina el elemento de la lista
		printf("Se ha desasignado correctamente el segmento.\n");
		return 0;
	}
}

/*
Funcion que busca en todas las listas y desasigna memoria en funcion de la direccion introducida
*/
int desasigna_direccion(char ** opciones){

	if (opciones[0] == NULL) mostrar_mem();
	else{
		if ((desasigna_malloc(opciones,1)) != -1) return 0;
		if ((desmapea_mmap(opciones,1)) != -1) return 0;
		if ((desmapea_shared(opciones,1)) != -1) return 0;
	
		printf("La dirección introduccida no ha sido reservada en memoria\n");
		return -1;
	}
}

/*------------------------------------
Opcion DEASSIGN
*/
void opcion_deassign(char ** opciones){	
	if (opciones[0] == NULL)  mostrar_mem();
	else if(!strcmp(opciones[0], "malloc")) desasigna_malloc(opciones+1,0);
	else if(!strcmp(opciones[0], "mmap")) desmapea_mmap(opciones+1,0);
	else if(!strcmp(opciones[0], "shared")) desmapea_shared(opciones+1,0);
	else desasigna_direccion(opciones);
}

/*
Funcion principal MEM
*/
void mem(char ** opciones){

	if (opciones[0] == NULL) mostrar_mem();
	else if (!strcmp(opciones[0], "-assign"))	opcion_assign(opciones+1);
    else if (!strcmp(opciones[0], "-deassign"))	opcion_deassign(opciones+1);
	else if (!strcmp(opciones[0], "malloc"))	mostrar_malloc(); 
	else if (!strcmp(opciones[0], "mmap"))		mostrar_mmap(); 
	else if (!strcmp(opciones[0], "shared"))	mostrar_shared(); 
	else printf("La opción introducida no es correcta.\n");
}

//-----------------------------------------------------------------------------//
//--------------------------------FUNCIONES MEMIO------------------------------//
//-----------------------------------------------------------------------------//
/*
COPIAR A MEMORIA: Función que lee un archivo en memoria
*/
int copia_a_memoria(char ** opciones){
	if (opciones[0] == NULL || opciones[1] == NULL) {
		printf("Argumentos insuficientes.\n");
		return -1;
	}
	char * filename = opciones[0];
	void * buff;
	int fd,statdest,tam,direccion;
	struct stat estructura;

	if ((direccion = parse_hex(opciones[1])) == -1) return -1;//Extrae la dirección y comprueba si hay error
	
    if ((fd = open(filename,O_RDONLY))== -1) { //extrae el file_descriptor
		perror("Error abriendo el archivo para lectura");
		return -1;
   	}
	if ((statdest = stat(filename,&estructura)) < 0) { //comprobamos que stat no diese ningun error
		(errno == ENOENT) ? perror("El archivo no existe.") : perror("Accediendo al archivo.");
		close(fd);
		return -1;
	}
	tam = estructura.st_size; //extrae el tamaño
	buff = (void *) direccion;
	if (read(fd,buff,tam) < 0){
		perror("Error leyendo el archivo en memoria");
		close(fd);
		return -1;
	}
	close(fd);
	return 0;
}

/*
COPIAR A ARCHIVO: Función que copia desde una dirección de memoria a un fichero
*/
int copia_a_archivo(char ** opciones){
	if (opciones[0] == NULL || opciones[1] == NULL || opciones[2] == NULL) {
		printf("Argumentos insuficientes.\n");
		return -1;
	}
	char * filename = opciones[0];//extrae el nombre del archivo
	int count, fd, sobrescribe = 0, flag;
	void * buff;

	if ((buff = (void *) parse_hex(opciones[1])) == (void*) -1) return -1;//Extrae la dirección y comprueba si hay error
	if ((count = parse_dec(opciones[2])) == -1) return -1;//extrae count

	if (opciones[3] != NULL) sobrescribe = (!strcmp("-o",opciones[3]));//extrae si se sobreescribe o no
	flag = (sobrescribe) ? O_TRUNC:O_CREAT;//Selecciona el flag
	if ((fd = open(filename,(O_WRONLY | flag),S_IRWXU)) == -1) {//abre el archivo para escritura
		perror("Error abriendo el archivo para escritura");
		return -1;
   	}
	if ((write(fd, buff, count)) == -1) { //Escribe el fichero
		perror("Error escribiendo en el archivo");
		close(fd);
		return -1;
	}	
	close(fd);
	return 0;
}

/*
Funcion principal IOMEM
*/
void iomem(char ** opciones){
	if (opciones[0] == NULL) printf("Argumentos insuficientes\n");
	else if(!strcmp(opciones[0], "-read")) copia_a_memoria(opciones+1);
	else if(!strcmp(opciones[0], "-write")) copia_a_archivo(opciones+1);
	else printf("La opción introducida no es correcta.\n");
}

//-----------------------------------------------------------------------------//
//------------------------------FUNCIONES MEMDUMP------------------------------//
//-----------------------------------------------------------------------------//

void print_mem(char * dir, int n){
    int i, chr = 1;
    
    for(i=0; i<n; i++){
    	if (chr){
			if ((unsigned char) dir[i] < '!') printf("   ");
			else printf(" %2c",dir[i]);
        }else printf(" %2X",(unsigned char) dir[i]);
        
        if (((i+1) % MEMDUMP_LINEA) == 0 || ((i+1) == n && (n % MEMDUMP_LINEA) != 0)){
            if (chr) i -= (i+1) % MEMDUMP_LINEA ? n % MEMDUMP_LINEA : MEMDUMP_LINEA ;
            
            chr = ! chr;
            printf("\n");
        }
    }

}

void memdump(char ** opciones){
    char * dir;
    int n;

    if (opciones[0] == NULL){
    }
    
    if ((dir = (char*) parse_hex(opciones[0])) == (void*) -1) return;
    
    if ((n = opciones[1] == NULL ? MEMDUMP_LINEA : parse_dec(opciones[1])) < 0) return;
    
    print_mem(dir,n); 

}

//-----------------------------------------------------------------------------//
//-----------------------------FUNCIONES RECURSE-------------------------------//
//-----------------------------------------------------------------------------//


void recursiva (int n, int liberar, int delay){
    char automatico[1024]; 
    static char estatico[1024]; 
    void * puntero;

    puntero=(void *) malloc (1024);
    printf ("parametro n:%d en %p\n",n,&n);
    printf ("valor puntero:%p en direccion: %p\n", puntero,&puntero);
    printf ("array estatico en:%p \n",estatico);
    printf ("array automatico en %p\n",automatico);
    if (liberar==REC_ANTES)
       free (puntero);
    if (n>0)
       recursiva(n-1,liberar,delay);
    if (liberar==REC_DESPUES)
       free (puntero);
    if (n==0 && delay)        /* espera en la ultima recursividad*/  
       sleep (delay);
}

void recurse(char ** opciones){

    int n, liberar, delay;

    if (opciones[0] == NULL){
        printf("Usage: recurse [-a|-n|-d] [-sN] n \n");
        return;
    }else if (!strcmp(opciones[0],"-d")){
        liberar = REC_DESPUES;
        opciones++;
    }else if (!strcmp(opciones[0],"-n")){
        liberar = REC_NO;
        opciones++;
    }else if (!strcmp(opciones[0],"-a")){
        liberar = REC_ANTES;
        opciones++;
    }else{ 
        liberar = REC_DESPUES;
    }
    
     if (!strncmp(opciones[0],"-s",2)){
        if ( (delay = parse_dec(opciones[0] + 2)) < 0) return;
        opciones++;
    }else{
        delay = 0;
    }
    
    if((n = parse_dec(opciones[0])) < 0 ) return;
    
    recursiva(n,liberar,delay);
    
}

//-----------------------------------------------------------------------------//
//------------------------------FUNCIONES MEMFS--------------------------------//
//-----------------------------------------------------------------------------//

struct memfs_item{
    time_t time;
    int tam;
    char name[MAX_NAME];
};

struct memfs_data{
	long unsigned int marca;
   time_t time;
	int tam;
	int espaciolibre;
	int numero_archivos;
};

void separar_mem_fich(char *id, char ** idmem, char ** idfich){
    char * guion;
    
    if((guion = index(id,'-')) == NULL || *(guion + 1) != '>'){
        *idmem  = NULL;
        *idfich = id;
    }else{
        guion[0] = '\0';
        guion[1] = '\0';
        *idmem  = id;
        *idfich = guion + 2;
    }
}

struct memfs_item * get_fichero_fs(struct memfs_data * memid, char* fich){
    int i;
    struct memfs_item * item = (struct memfs_item *)((void*) memid + sizeof(struct memfs_data));
    
    for(i = 0; i < memid->numero_archivos ; i++){
        if (!strcmp(item->name,fich)) return item;
        item = (struct memfs_item *)((void*) item + item->tam);
    }
    errno = ENOENT;
    return NULL;
}

void * get_espacio_libre(struct memfs_data * memid, int tam){
    if(memid->espaciolibre < tam){
        errno = ENOMEM;
        return NULL;
    }
    return (void*) memid + memid->tam - memid->espaciolibre;
}

struct memfs_item * get_memfs_fich_struct(char* name, unsigned int tam){
    static struct memfs_item item;
    if(strlen(name) >= MAX_NAME) return NULL;
    memcpy(item.name, name, sizeof(char) * (strlen(name)+1));
    item.tam = tam + sizeof(struct memfs_item);
    item.time = time(NULL);
    return &item;
}

//Funcion que reserva un segmento de memoria compartida y lo inserta en la lista
void * get_shared_mem(char* id, int* tam){
	int key, flags;
	key_t shmid;
	struct shmid_ds s;
	void * reserva;
	
	if ((key = parse_dec(id)) == -1) return NULL; //extrae key y comprueba errores
	
	flags = (*tam ? IPC_CREAT | IPC_EXCL : 0 )| S_IRWXU;
	if ((shmid = shmget(key,*tam,flags)) == -1) return NULL;
	
	if(!*tam){
	    if (shmctl(shmid,IPC_STAT,&s) == -1) return NULL;
		else *tam = s.shm_segsz;
	}
	
	if ((reserva = (int*) shmat(shmid,0,0)) == (void *) -1) return NULL;

	if (inserta_e_mem(lista_shared,LISTA_SHARED,*tam,reserva,NULL) < 0) return NULL;
	
	return reserva;
}


/*Funcion que busca en las listas MALLOC Y SHARED la direccion y en caso de no encontrarla
crea un nuevo segmento de memoria compartida.
*/
void * obtener_mem(char* id, int*tam){

	int pos1, pos2;
	void * dir;  	
	e_mem p, * elemento;
	
  	dir = (void *) parse_hex(id); //Extrae la direccion
	p.direccion = dir;
	
	pos1 = buscarElemento(lista_malloc,LISTA_MALLOC,&p,1);
	pos2 = buscarElemento(lista_shared,LISTA_SHARED,&p,1);
	
	if (pos1 != -1 || pos2 != -1){
	    elemento = (e_mem *) getElemento(pos1 != -1 ? lista_malloc : lista_shared,pos1 != -1 ? pos1 : pos2);
		*tam = elemento->tamanho;
		return dir;
	}else{
	    *tam = 0; 
	    return get_shared_mem(id,tam);
	}
	
}

struct memfs_data * obtener_dir_fs(char* memid){
    int tam = 0;
    struct memfs_data* dir = obtener_mem(memid, &tam);
    
    if (dir != NULL && dir->marca != MARCA_MEMFS){
        printf("Error la direccion indicada no es un sitema de ficheros en memoria.\n");
        return NULL;
    }
    
    return dir;
}


//COPY...

int copy_fs_disc(char* idfich1, char* idfich2){
    static char buffer[BUFFER_SIZE];
    int idorigen, iddest, tamr, tamw;
    
    //Abre los archivos origen y destino.
    idorigen = open(idfich1,O_RDONLY);
    iddest = open(idfich2,O_WRONLY | O_CREAT | O_EXCL, 00600);
        
    //Copia la informacion del archivo origen en el destino.
    if( idorigen != -1 && iddest != -1){    
        do{
            if((tamr = read(idorigen,buffer,BUFFER_SIZE))  == -1) break;
            if((tamw = write(iddest,buffer,tamr)) == -1) break;
        }while(tamw == BUFFER_SIZE);
    }
    
    if(iddest != -1) close(iddest);
    if(idorigen != -1) close(idorigen);
    return idorigen != -1 && iddest != -1 && tamr != -1 && tamw != -1 ? 0 : -1;
}

int copy_fs_memtodisc(char * namefichdisc, char* memid, char* idfichmem){
    struct memfs_item * item; 
    struct memfs_data * dir_fs;
    int iddest;
    
    //Conseguimos el item a copiar
    if((dir_fs = obtener_dir_fs(memid)) == NULL) return -1;
    if((item = get_fichero_fs(dir_fs,idfichmem)) == NULL) return -1;
    
    //Abrimos un archivo en modo escritura y escribimos lo que hay en memoria.
    if((iddest = open(namefichdisc,O_WRONLY | O_CREAT | O_EXCL, 00600)) == -1) return -1;
    write(iddest,(void*) item + sizeof(struct memfs_item), item->tam - sizeof(struct memfs_item));

    close(iddest);
    return 0;
}

int copy_fs_disctomem(char * namefichdisc, char* memid, char* namefichmem){
    struct memfs_item *newitem, *estructura;
    struct memfs_data * dir_fs;
    struct stat s;
    int idorigen;
    
    //Conseguimos el FS y comprobamos que el archivo no exista.
    if((dir_fs = obtener_dir_fs(memid)) == NULL) return -1;
    if(get_fichero_fs(dir_fs,namefichmem) != NULL){
        printf("Error copiando archivo. El archivo ya existe en el sistema de ficheros de destino.\n");
        return 1;
    }
    
    //Abrimos el fichero en modo lectura y conseguimos su tamaño.
    if((idorigen = open(namefichdisc,O_RDONLY)) == -1) return -1;
    if(fstat(idorigen,&s) == -1) return -1;
    
    if((newitem = get_espacio_libre(dir_fs,s.st_size + sizeof(struct memfs_item))) != NULL){
        //Copiamos la estructura del archivo y el archivo en si en el espacio dado.
        if((estructura = get_memfs_fich_struct(namefichmem,s.st_size)) == NULL){
        		printf("Error, nombre demasiado grande.\n");
        		return -1;
        }
        
		  memcpy(newitem, estructura, sizeof(struct memfs_item));
        read(idorigen,(void*) newitem + sizeof(struct memfs_item),s.st_size);
    
        dir_fs->espaciolibre -= s.st_size + sizeof(struct memfs_item);
        dir_fs->numero_archivos++;
    }
    
    close(idorigen);
    return newitem == NULL ? -1 : 0;
}

//Copia de sitema de ficheros en memoria a sistema de ficheros en memoria.
int copy_fs_mem(char* idmem1, char* idfich1, char* idmem2, char* idfich2){
    struct memfs_item * item, * newitem;
    struct memfs_data * dir_fs1, * dir_fs2;
    
    //Conseguimos los dos FS en memoria.
    if((dir_fs1 = obtener_dir_fs(idmem1)) == NULL) return -1;
    if((dir_fs2 = obtener_dir_fs(idmem2)) == NULL) return -1;

    //Consigue el archivo a copiar y comprueba que el nombre no existe en el FS destino.
    if((item = get_fichero_fs(dir_fs1,idfich1)) == NULL) return -1;
    if(get_fichero_fs(dir_fs2,idfich2) != NULL){
        printf("Error copiando archivo. El archivo ya existe en el sistema de ficheros de destino.\n");
        return 1;
    }
    
    //Consigue un espacio libre en el FS destino.
    if((newitem = get_espacio_libre(dir_fs2,item->tam)) == NULL) return -1;
    
    //Copiamos la estructura del archivo y el archivo en si en el espacio dado.
    memcpy(newitem,item,item->tam);
    newitem->time = time(NULL);
    strcpy(newitem->name,idfich2);
    
    dir_fs2->espaciolibre -= item->tam ;
    dir_fs2->numero_archivos++;
    return 0;
}

int memfs_cp(char ** opciones){
    int disco1, disco2;
    char * idmem1, *idfich1, *idmem2, *idfich2;
    int valor_retorno;
    
    if (opciones[0] == NULL || opciones[1] == NULL){
        printf("Usage...");
        return -1;
    }
    
    separar_mem_fich(opciones[0],&idmem1,&idfich1);
    separar_mem_fich(opciones[1],&idmem2,&idfich2);
    
    disco1 = idmem1 == NULL || !strcmp(idmem1,"disk");
    disco2 = idmem2 == NULL || !strcmp(idmem2,"disk");

    if (disco1 && disco2)       valor_retorno = copy_fs_disc(idfich1, idfich2);
    else if (disco1 && !disco2) valor_retorno = copy_fs_disctomem(idfich1, idmem2, idfich2);
    else if (!disco1 && disco2) valor_retorno = copy_fs_memtodisc(idfich2, idmem1, idfich1);
    else                       valor_retorno = copy_fs_mem(idmem1, idfich1, idmem2, idfich2);

    if(valor_retorno == -1) perror("Error copiando");
    return valor_retorno;
}


//RM...

int rm_fs_item_disc(char * idfich){
    return unlink(idfich);
}

int rm_fs_item_mem(char *dir_fs, char * idfich){
    struct memfs_item * item;
    struct memfs_data * memdir;
    int tam;
    void * origen;
    void * destino;
    
    if((memdir = obtener_dir_fs(dir_fs)) == NULL) return -1;
    if((item = get_fichero_fs(memdir,idfich)) == NULL) return -1;
//    free(item->name);
    
    tam = item->tam;
    
    //O tamaño e igual a posicion final do sistema de ficheiros menos a posicion donde se empeza a copiar.
    destino=origen=item;
    origen+=item->tam;
    origen=memdir;
    memcpy(item,(void*)item + item->tam, 
        (size_t) ((void*)memdir + memdir->tam - memdir->espaciolibre - (void *) item - item->tam));
    
    memdir->espaciolibre += tam;
    memdir->numero_archivos--;
    
    return 0;
}

int memfs_rm(char ** opciones){
    char *idmem, *idfich;
    int valor_retorno;
    
    if (opciones[0] == NULL){
        printf("Usage...");
        return 1;
    }

    separar_mem_fich(opciones[0],&idmem,&idfich);
    
    valor_retorno = idmem == NULL || !strcmp(idmem,"disc") ?  rm_fs_item_disc(idfich) : rm_fs_item_mem(idmem,idfich); 
    
    if (valor_retorno == -1){
        perror("Error eliminando");
        return 1;
    }
    return 0;
}

//MV -> CP -> RM
int memfs_mv(char ** opciones){
    static char* opcionesrm[1];
    int ret_cp, ret_rm;

    if (opciones[0] == NULL || opciones[1] == NULL){
        printf("Usage: memfs-mv fich1 fich2");
        return 1;
    }else{
        opcionesrm[0] = strdup(opciones[0]);
        ret_cp = memfs_cp(opciones);
        if(!ret_cp) ret_rm = memfs_rm(opcionesrm);
        
        free(opcionesrm[0]);
        if(ret_cp || ret_rm) perror("Error moviendo");
        return ret_cp || ret_rm ? -1 : 0;
    }
}

//LS..
int memfs_ls(char ** opciones){
    struct memfs_data * memid;
    struct memfs_item * item;
    int i;

    if (opciones[0] == NULL) {
        printf("Usage:memfs-ls dir");
        return -1;
    }else{

        if((memid = obtener_dir_fs(opciones[0])) == NULL){
            perror("Error obteniendo sistema de ficheros");
            return -1;
        }

        printf("Sistema de ficheros con una capacidad de %i creado con fecha %s",memid->tam,ctime(&(memid->time)));
        printf(" El espacio disponible es %i y el numero de ficheros %i.\n",memid->espaciolibre,memid->numero_archivos); 

        item = (struct memfs_item*) ((void*) memid + sizeof(struct memfs_data));

        for(i=0; i < memid->numero_archivos; i++){
            printf(" - Nombre:%s Tamaño:%i Fecha de creación:%s",item->name,item->tam, ctime(&(item->time)));
            item = (struct memfs_item*) ((void*) item + item->tam);
        }
        return 0;
    }
}


//MAKE FS

//Funcion que crea el sistema de ficheros
void create_fs(void * mem, int tam){
	if (tam < sizeof(struct memfs_data)){
		//Error no hay memoria suficiente.
	}else{
		struct memfs_data s;
		s.time = time(NULL);
		s.tam = tam;
		s.espaciolibre = tam - sizeof(struct memfs_data);
		s.numero_archivos = 0;
		s.marca =  MARCA_MEMFS;
		memcpy(mem, &s, sizeof(struct memfs_data));
	}
}

/*
Función principal de MEMFS_MK
*/
void memfs_mk(char ** opciones){

	void  * mem;
	int tam;

	if (opciones[0] == NULL){ //Comprueba si tiene id_mem
		printf("Usage: mkmemfs id_mem [tam]\n");
		return;	
	}
	if (opciones[1] == NULL) //Comprueba si hay tamaño
		mem = obtener_mem(opciones[0],&tam); //Busca si la posicion esta mapeada
	else{
		if((tam = parse_dec(opciones[1])) < 0) return;//Obtiene el tamaño y comprueba errores
		mem = get_shared_mem(opciones[0],&tam); //Reserva la memoria
	}
	
	if (mem == NULL) perror("Error obteniendo memoria");
	else create_fs(mem, tam);
}


//-----------------------------------------------------------------------------//
//------------------------------FUNCIONES RMKEY--------------------------------//
//-----------------------------------------------------------------------------//

/*
Funcion principal RMKEY
*/
int rmkey(char ** opciones){

		int key;
		key_t shmid;
		void * reserva;
		
		if ((key = parse_dec(opciones[0])) < 0) return -1; //extrae key
		
		if ((shmid = shmget(key,0,0)) < 0){//obtiene la memoria compartida
			perror("Error obteniendo segmento de memoria compartida.");
			return -1;
		}

		if(shmctl(shmid, IPC_RMID, 0) == -1){
			perror("Error en el borrado del segmento.");
			return -1;
		 }
		 printf("Eliminado el segmento de memoria compartida con la key: (%i)\n",key);
		 return 0;
}


	//-----------------------------------------------------------------------------//
	//------------------------------------ SIGINFO --------------------------------//
	//-----------------------------------------------------------------------------//

	short * get_sen_args(char ** opciones){
	    static short array_mostrar[32];
	    int i, sen;
	    
	    if(opciones[0] == NULL){
		    for(i = 0; i < 32; i++) array_mostrar[i] = 1;
	    }else{
		    for(i = 0; i < 32; i++) array_mostrar[i] = 0;
		    do{
                if((sen = Senal(opciones[0])) == -1){
		            printf("Señal %s desconocida.\n",opciones[0]);
		            return NULL;
		        }else{
                    array_mostrar[sen] = 1;
		        }
		        opciones++;
		    }while(opciones[0] != NULL);
	    }
	    return array_mostrar;
	}

	void mostrar_info_senal(int numero_senal){
	    struct sigaction s;
	    int i, nomask=1;
	    
	    if(sigaction(numero_senal, NULL, &s) == -1){
		    perror("Error consiguiendo datos de señal.");
	    }else{
		    printf("%s:",NombreSenal(numero_senal));

		if(s.sa_handler == SIG_DFL)
		     printf("Default");
		else if (s.sa_handler == SIG_IGN)
		     printf("Ignored");
		else
		    printf("Handler en %p ", s.sa_handler);

		if(SA_NODEFER & s.sa_flags)
		    printf(" NODEFER");
		if(SA_ONSTACK & s.sa_flags)
		    printf(" ONSTACK");  
		if(SA_RESETHAND & s.sa_flags)
		    printf(" RESETHAND");
		if(SA_RESTART & s.sa_flags)
		    printf(" RESTART");
		    
		if(sigparametros[numero_senal].showhandler)
		    printf(" -show");
		if(sigparametros[numero_senal].resendhandler)
		    printf(" -resend");
		if(sigparametros[numero_senal].sleeptimehandler != 0)
		    printf(" -time: %i",sigparametros[numero_senal].sleeptimehandler);
		
		printf("\n   Mascara de la señal:");
	    
		for(i=1; i<=31; i++)
		    if(sigismember(&(s.sa_mask),i) == 1){
			printf("%s ",NombreSenal(i));
			nomask = 0;        
		    }
		printf("%s",nomask ? "None\n" : "\n");
	    
	    }
	}

	void infosenales(char ** opciones){
	    short * array;
	    int i;
	    
	    if((array = get_sen_args(opciones))==NULL)
	        return;
	    
	    for(i=1; i<=31; i++){
		if(array[i])
		    mostrar_info_senal(i);
	    }
	}



	//-----------------------------------------------------------------------------//
	//---------------------------------- SIGPROCMASK ------------------------------//
	//-----------------------------------------------------------------------------//

	//Funcion que muestra las señales que se encuentran bloqueadas
	void mostrar_bloqueo(){
       	sigset_t old_mask;
        int i;
		if (sigprocmask(SIG_BLOCK,NULL,&old_mask) == -1){ //Obtiene las señales bloqueadas
			perror("Error bloqueando/desbloqueando las señales.");
			return;
        }
        printf("Señales bloqueadas:\n");
        for(i=1; i<=31; i++){
			if(sigismember(&old_mask, i))//Imprime las que coinciden con las obtenidas
				mostrar_info_senal(i);
		}
	}
	
	void sigprocmask_sen(char ** opciones){
		int how,i,show=0;
       	sigset_t mask;
       	short * array;
       	
		if (opciones[0] == NULL) {mostrar_bloqueo(); return;}
		else if (!strcmp(opciones[0], "-block"))	how = SIG_BLOCK;
	    else if (!strcmp(opciones[0], "-unblock"))	how = SIG_UNBLOCK;
		else if (!strcmp(opciones[0], "-setmask"))	how = SIG_SETMASK; 
		else {printf("La opción introducida no es correcta.\n"); return;}

	   if((array = get_sen_args(opciones+1))==NULL) return;//Crea el array con las señales introducidas como parametro
		sigemptyset(&mask);//Inicializa la mascara

		for(i=1; i<=31; i++){ //Añade las señales a la mascara
			if(array[i])
				sigaddset(&mask, i);
		}
		if (sigprocmask(how,&mask,NULL) == -1){//Bloquea o desbloquea las señales
			perror("Error bloqueando/desbloqueando las señales.");
			return;
		}
	}

	//-----------------------------------------------------------------------------//
	//---------------------------------- SIGALTSTACK ------------------------------//
	//-----------------------------------------------------------------------------//

	void mostrarpila(){
	    stack_t s;
	    if (sigaltstack(NULL, &s) == -1)
		perror("Error obteniendo pila de señales");
	    else
		printf("Pila situada en la direccion %p con tamaño %i Bytes.\n",s.ss_sp,s.ss_size);

	}

	void setpila(void* dir, size_t tam){
	    stack_t s;
	    
	    s.ss_sp = dir;
	    s.ss_size = tam;
		
	    if(sigaltstack(&s,NULL) == -1)
		    perror("Error cambiando la pila");

	}

	void setpilaalternativa(char ** opciones){
	    void * dir;
	    size_t tam;

	    if(opciones[0] == NULL){
		mostrarpila();
	    }else{ 
		if ((tam=parse_dec(opciones[0])) == -1){
		    return;
		}else if (opciones[1] == NULL){
		    if((dir = malloc(tam)) == NULL){
			perror("Error reservando memoria para pila");
			return;
		    }
		}else if ((dir = (void*) parse_hex(opciones[1])) == (void*) -1){
		    return;
		}
		    
		setpila(dir,tam);
	    }
	}

	//-----------------------------------------------------------------------------//
	//----------------------------------- SIGACTION -------------------------------//
	//-----------------------------------------------------------------------------//
    
    void customhandler (int senal){
    
        if (senal<0 || senal>31){
            printf("Error senal no reconocida");
            return;
        }
        
        if (sigparametros[senal].showhandler)
            printf("Señal %s recibida. Ya van %i veces que se maneja esta señal.\n"
            "\tEl parametro esta en al dirección %p.\n",NombreSenal(senal),++(sigparametros
            [senal].counthandler), &senal);    
        
        
        if (sigparametros[senal].resendhandler &&
            (++countsigact < limitresend || limitresend == -1))
            kill(getpid(),senal);
        else
            countsigact = 0;
        
        if (sigparametros[senal].sleeptimehandler)
            sleep(sigparametros[senal].sleeptimehandler);
    }
    
    void mostrar_senales(void (*handler)(int)){
        struct sigaction s;
        int i;
        
        for (i=1; i<32; i++){
            if(sigaction(i,NULL,&s) == -1){
                perror("Error obteniendo datos de señal.");
                break;
            }
            
            if(s.sa_handler == handler)
                mostrar_info_senal(i); 
        }
    }  


    void sethandler(void (*handler)(int), short * array, int flags, int show,
                    int resend, sigset_t * mask, int time)
    {
        struct sigaction s;
        int i;
        
        s.sa_handler = handler;
        s.sa_mask = *mask;
        s.sa_flags = flags;
        
        for(i = 1; i < 32; i++){
            if(array[i]){
                if(sigaction(i, &s, NULL) == -1){
                    perror("Error...");
                    return;
                }
                sigparametros[i].sleeptimehandler = time;
                sigparametros[i].showhandler = show;
                sigparametros[i].resendhandler = resend;
            }
        }   
    }

    void setsignal_hand(char** senales){
        short * array;
        int i, flags=0, show=0, resend=0, sen=0, time=0;
        sigset_t mask;
    
	    sigemptyset(&mask);
        for(; *senales != NULL; senales++)
            if (!strcmp(senales[0],"-show")){
                show = 1;
            }else if (!strcmp(senales[0],"-resend")){
                resend = 1;
            }else if (!strcmp(senales[0],"-resethand")){
                flags = flags | SA_RESETHAND;
            }else if (!strcmp(senales[0],"-onstack")){
                flags = flags | SA_ONSTACK;
            }else if (!strcmp(senales[0],"-nodefer")){
                flags = flags | SA_NODEFER;
            }else if (!strcmp(senales[0],"-restart")){
                flags = flags | SA_RESTART;
            }else if (!strncmp(senales[0],"-maskSEN",5)){
                if((sen = Senal(senales[0] + 5)) == -1){
                    printf("Señal %s no reconocida",senales[0] + 5);
                    return;
                }else{ 
                    if(sigaddset(&(mask), sen) == -1){
                        perror("Error añadiendo señal a mascara");
                        return;
                    }
                }
            }else if (!strncmp(senales[0],"-sleepNN",6)){
                if((time = parse_dec(senales[0]+6)) < 0){
                    printf("Error tiempo inadecuado");
                    return;
                }
            }else{  //Se entiende que a partir de aqui son todo señales.
                if((array = get_sen_args(senales))== NULL)
                    printf("Error reconociendo señales\n");
                else
                    sethandler(customhandler, array, flags, show, resend, &mask, time);
                return;
            }
        printf("Usage: sigaction [-defaultl|-ignore|-handler] [-show] [-resend] [-resethand] [-onstack] [-nodefer] [-restart] [-sleepNN] [-maskSEN1] [-maskSEN2] ... S1 S2\n");
    }
	
	void signalaction(char ** opciones){
	    sigset_t mask;
	    sigemptyset(&mask);
	    short * array;
	
		if(opciones[0] == NULL)
			infosenales(opciones);
		else if(!strcmp(opciones[0],"-default") || !strcmp(opciones[0],"-ignore"))
            if(opciones[1] == NULL)
                mostrar_senales(!strcmp(opciones[0],"-default") ? SIG_DFL : SIG_IGN);
            else if((array = get_sen_args(opciones+1))== NULL)
                printf("Error reconociendo señales");
            else
			    sethandler(!strcmp(opciones[0],"-default") ? SIG_DFL : SIG_IGN, 
			                array, 0, 0, 0, &mask, 0);
		else if(!strcmp(opciones[0],"-handler"))
			setsignal_hand(opciones+1);
		else
			infosenales(opciones);
	}

	//-----------------------------------------------------------------------------//
	//---------------------------------- LIMITRESEND ------------------------------//
	//-----------------------------------------------------------------------------//

	void setlimitresend(char ** opciones){
	    int num;
	    if(opciones[0] == NULL){
		if(limitresend == -1)
		    printf("Limit Resend: off\n");
		else
		    printf("Limit Resend: %i\n",limitresend); 
	    }else if(!strcmp(opciones[0],"off")){
		limitresend = -1;
	    }else if((num = parse_dec(opciones[0])) < 0){
		printf("Usage: limitresend [off|N]\n");
	    }else{
		limitresend = num;
	    }
	}


	//-----------------------------------------------------------------------------//
	//------------------------------------ SIGCOUNT -------------------------------//
	//-----------------------------------------------------------------------------//

	void mostrar_sigcount(short array_mostrar[32]){
	    int i;
	    
	    for(i = 1; i < 32; i++)
		if(array_mostrar[i])
		    printf("%s : %i\n",NombreSenal(i),sigparametros[i].counthandler);
	}

	void set_zero_sigcount(short array_mostrar[32]){
	    int i;
	    
	    for(i = 1; i < 32; i++)
		if(array_mostrar[i])
		    sigparametros[i].counthandler = 0;
	}

	void sigcount(char ** opciones){
	    int setzero=0;
	    short * array;
	    
	    if(opciones[0] != NULL && !strcmp(opciones[0],"-zero")){
		    setzero = 1;
		    opciones++;
	    }

	    if((array = get_sen_args(opciones))==NULL) return;

	    if(setzero) set_zero_sigcount(array);
	    else        mostrar_sigcount(array);
	}

	//-----------------------------------------------------------------------------//
	//------------------------------------- BUCLE ---------------------------------//
	//-----------------------------------------------------------------------------//

     void bucleinfinito(){
        char buffer[100];
        static short array[32];
        int i;
        sigset_t mask;
	    sigemptyset(&mask);
        
        for(i=1; i<32; i++) array[i] = 0;
        array[Senal("INT")] = 1;
        sethandler(customhandler, array, 0, 1, 0, &mask, 0);
    
        if (sigaddset(&mask,Senal("INT")) == -1){
            perror("Error añadiendo señal INT a conjunto de señales");
            return;
        }
    
        if (sigprocmask(SIG_UNBLOCK,&mask,NULL) == -1){
			perror("Error bloqueando INT.");
			return;
		}
    
        while(fgets(buffer,100,stdin) != NULL);
    }    


	//-----------------------------------------------------------------------------//
	//--------------------------------- SEGMENTATION ------------------------------//
	//-----------------------------------------------------------------------------//

	void segmentation(){
		*((int*) NULL) = 1;
	}

	//-----------------------------------------------------------------------------//
	//-------------------------------------- UID ----------------------------------//
	//-----------------------------------------------------------------------------//


	int mostrar_uid(){
	    uid_t real_uid, efec_uid;
	    struct passwd * s;

	    real_uid = getuid();
	    efec_uid = geteuid();
	    
	    if((s = getpwuid(efec_uid)) == NULL) return -1;
	    printf("Credencial real %s (%i)\n",s->pw_name, s->pw_uid);
	    
	    if((s = getpwuid(real_uid)) == NULL) return -1;
	    printf("Credencial efectiva %s (%i)\n",s->pw_name,s->pw_uid);
	}

	void userid(char ** opciones){
	    struct passwd * s;
	    int cred;

		if(opciones[0] == NULL){
		   if (mostrar_uid() == -1) perror("Error mostrando credenciales");
		}else{
		    if (!strcmp(opciones[0],"-l")){
			    if(opciones[1] == NULL){
				printf("Usage:...");
				return;
			    }else if((s = getpwnam(opciones[1])) == NULL){
				perror("Error obteniendo uid");
				return;
			    }else{
				cred = s->pw_uid;
			    }
		    }else if ((cred = parse_dec(opciones[0])) == -1){
			return;
		    }
		
		    if(setuid(cred) == -1)
			perror("Error cambiando credenciales:");
		}
	}

	//-----------------------------------------------------------------------------//
	//----------------------------------- PIPE ------------------------------------//
	//-----------------------------------------------------------------------------//
	
	
	hacer_pipe(char ** args1, char ** args2, int segundoplano){
	    int df[2];
	    pid_t p1,p2;
	    
	    if(pipe(df) == -1)
	        return -1;
	    
	    if((p1 = fork()) == 0){
	        close(1);
	        dup(df[1]);
	        
	        //Executase o comando sen crear proceso(xa se creou) e
	        //     en segundo plano(esperarase aquí).
	        ejecutar_programa_externo(args1, 0, 1);
	    
	        //Se falla ejecutar programa externo terminase o proceso.
	        exit(0);
	    }
	    
	    close(df[1]);
	    
	    if((p2=fork()) == 0){
	        close(0);
	        dup(df[0]);
	        
	        ejecutar_programa_externo(args2, 0, 1);

	        exit(0);
	    }
	    
	    close(df[0]);
	    
	    if(!segundoplano){
	        waitpid(p1,NULL,0);
	        waitpid(p2,NULL,0);
	    }
	}

    void ejecutar_pipe(char ** opciones,int segundo_plano){
        char ** arg1 = opciones;
    
        for(; *opciones != NULL && strcmp(opciones[0],"%"); opciones++);
        
        if(opciones[0] != NULL && opciones[1] != NULL){
            opciones[0] = NULL;
            hacer_pipe(arg1, opciones+1, segundo_plano);
        }else{
            printf("Usage: pipe/backpipe prog1 [args1] %% prog2 [args2]\n");
        }
    }

	//-----------------------------------------------------------------------------//
	//-----------------------FUNCIONES PARSEAR COMANDO-----------------------------//
	//-----------------------------------------------------------------------------//

	/*
	Función que analiza el tipo de comando recibido y devuelve el correspondiente codigo en integer
	*/
	int comprueba_comando(char* com)
	{
		if ((!strcmp(com, "fin")) || (!strcmp(com, "exit")) || (!strcmp(com,"quit"))) return FIN;
		if (!strcmp(com, "autores")) return AUTORES ; 
		if (!strcmp(com, "pid")) return PID; 
		if (!strcmp(com, "pwd")) return PWD; 
		if (!strcmp(com, "cd")) return CD; 
		if (!strcmp(com, "path")) return PATH; 
		if (!strcmp(com, "environ")) return ENVIRON; 
		if (!strcmp(com, "fork")) return FORK; 
		if (!strcmp(com, "execute")) return EXECUTE; 
		if (!strcmp(com, "background")) return BACKGROUND;
		if (!strcmp(com, "jobs")) return JOBS; 
		if (!strcmp(com, "mem")) return MEM;
	    if (!strcmp(com, "iomem")) return IOMEM;
	    if (!strcmp(com, "memdump")) return MEMDUMP;
	    if (!strcmp(com, "recurse")) return RECURSE;
	    if (!strcmp(com, "mkmemfs")) return MEMFSMK;
	    if (!strcmp(com, "memfs-cp")) return MEMFSCP;
	    if (!strcmp(com, "memfs-mv")) return MEMFSMV;
	    if (!strcmp(com, "memfs-rm")) return MEMFSRM;
	    if (!strcmp(com, "memfs-ls")) return MEMFSLS;
	    if (!strcmp(com, "rmkey")) return RMKEY;
	    if (!strcmp(com, "priority")) return PRIORITY;
	    if (!strcmp(com, "posixpri")) return POSIXPRI;
	    if (!strcmp(com, "sigprocmask")) return SIGPROCMASK;
	    if (!strcmp(com, "sigaltstack")) return SIGALTSTACK;
	    if (!strcmp(com, "sigaction")) return SIGACTION;
	    if (!strcmp(com, "limitresend")) return LIMITRESEND;
	    if (!strcmp(com, "siginfo")) return SIGINFO;
	    if (!strcmp(com, "sigcount")) return SIGCOUNT;
	    if (!strcmp(com, "bucle")) return BUCLE;
	    if (!strcmp(com, "segmentation")) return SEGMENTATION;
	    if (!strcmp(com, "uid")) return UID;
	    if (!strcmp(com, "pipe")) return PIPE;
	    if (!strcmp(com, "backpipe")) return BACKPIPE;
		return COMMAND_NOT_FOUND;
	}

	/*
	Parsea la cadena que corresponde a un comando de la shell y lo ejecuta.
	*/
	void analizaryejecutarcomando(char* com, char *env[])
	{
		char * opciones[MAXCOMANDO/2];
		int i;
		
		opciones[0] = strtok(com, " \t\n");
		
		if (opciones[0] == NULL) return;
	   
	    for(i = 1; (opciones[i] = strtok(NULL, " \t\n")) != NULL; i++);
			
		switch(comprueba_comando(opciones[0]))
		{
			case FIN: salir(); break;	
			case AUTORES: autores(); break;	
			case PID: pid(); break;	
			case PWD: pwd(); break;		
			case ENVIRON: environ_f(opciones+1,env); break;
			case CD: change_directory(opciones+1);	break;	
			case PATH: path(opciones+1); break;	
			case FORK: shell_fork(); break;	
			case EXECUTE: execute(opciones+1); break;	
			case BACKGROUND: background(opciones+1); break;	
			case JOBS: jobs(opciones+1); break;	
			case MEM: mem(opciones+1); break;
		case IOMEM: iomem(opciones+1); break;
		case MEMDUMP: memdump(opciones+1); break;
		case RECURSE: recurse(opciones+1); break;
		case MEMFSMK: memfs_mk(opciones+1); break;
		case MEMFSCP: memfs_cp(opciones+1); break;
		case MEMFSMV: memfs_mv(opciones+1); break;
		case MEMFSRM: memfs_rm(opciones+1); break;
		case MEMFSLS: memfs_ls(opciones+1); break;
		case RMKEY: rmkey(opciones+1); break;
		case PRIORITY: priority(opciones+1); break;
		case POSIXPRI: posixpri(opciones+1); break;
		case SIGPROCMASK: sigprocmask_sen(opciones+1); break;
		case SIGALTSTACK: setpilaalternativa(opciones+1); break;
		case SIGACTION: signalaction(opciones+1); break;
		case SIGINFO: infosenales(opciones+1); break;
        case SIGCOUNT: sigcount(opciones+1); break;
        case LIMITRESEND: setlimitresend(opciones+1); break;
        case BUCLE: bucleinfinito(); break;
        case SEGMENTATION: segmentation(); break;
        case UID: userid(opciones+1); break;
        case PIPE: ejecutar_pipe(opciones+1,0); break;
        case BACKPIPE: ejecutar_pipe(opciones+1,1); break;
		case COMMAND_NOT_FOUND: ejecutar_programa_externo(opciones, 1, 0); break;
	}
}

//-----------------------------------------------------------------------------//
//------------------------------FUNCION PRINCIPAL------------------------------//
//-----------------------------------------------------------------------------//


int main (int argc, char const *argv[],char *env[]){
	char comando[MAXCOMANDO];

    lista_path = nuevaLista();
    lista_jobs = nuevaLista();
    lista_malloc = nuevaLista();
    lista_mmap = nuevaLista();
    lista_shared = nuevaLista();
    mode_environ = T_ENVIRON;
    limitresend = -1;
    countsigact = 0;
    
	if (argc > 1)
		 redireccionar(argv[1],RE_INPUT);//Redireccionar entrada
		
		
	for(;;)
	{		

		printf("shell ~ ");
		if(fgets(comando,MAXCOMANDO,stdin) == NULL)
            perror ("Error al leer de teclado");

		analizaryejecutarcomando(comando,env);
	}
}
